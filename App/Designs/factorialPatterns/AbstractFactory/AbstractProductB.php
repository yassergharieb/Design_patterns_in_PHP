<?php 
namespace  App\Designs\factorialPatterns\AbstractFactory;
interface  AbstractProductA {
    
    public function doSomeThingInAnotherContext();
    
}